const initialState = {
  messages: [{
    id: 1,
    read: false,
    message: 'Where are you?',
  }, {
    id: 2,
    read: false,
    message: 'Where were you?',
  }],
  mockedMessages: [],
  currentMessage: {}
}

const messagereducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_MESSAGE':
      const forAdd = action.payload;
      console.log('ADD MESSAGE', action.payload);
      return {
        ...state,
        messages: state.messages.concat(forAdd),
      };
    case 'CHANGE':
      return {
        ...state,
        currentMessage: action.payload
      };
    case 'TOGGLE_MESSAGE':
      const list = state.messages;
      const index = action.payload;
      console.log('ovo je toggle message ', index);
      return {
        ...state,
        messages: list.slice(0, index)
          .concat([{ ...list[index], read: true }])
          .concat(list.slice(index + 1))
      };
    case 'DELETE_MESSAGE':
      const lista = state.messages;
      const id = action.payload;
      const newIndex = lista.findIndex(message => message.id === id);
      // console.log('ovo je novi index', newIndex);
      return {
        ...state,
        messages: lista.slice(0, newIndex)
          .concat(lista.slice(newIndex + 1))
      };
    case 'ALLTOREAD':
      return {
        ...state,
        messages: state.messages.map(x => {
          return Object.assign({}, x, {
            read: true
          })
        })
      };
    case 'FETCH_MESSAGES':
      // console.log('action.payload', action.payload);
      return {
        ...state,
        messages: action.payload
      }
    case 'FETCH_MOCKED_MESSAGES':
      console.log('ovoo je action payload', action.payload);
      return {
        ...state,
        mockedMessages: action.payload
      }
    default:
      return state;
  }
};
export default messagereducer;