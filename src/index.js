import React from 'react';
import  App  from '../src/containers/app';
import ReactDOM from 'react-dom';
import  { Provider } from 'react-redux';
import store from '../src/store';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,document.getElementById('root'));