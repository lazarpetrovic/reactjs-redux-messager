// import logger from 'redux-logger';
import {createStore, combineReducers, applyMiddleware } from 'redux';
import messagereducer from '../src/reducers/messagereducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
// import promise from 'redux-promise-middleware';

export default createStore(
  combineReducers({
    messages: messagereducer, 
    currentMessage: messagereducer,
    mockedMessages: messagereducer
  }),
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);