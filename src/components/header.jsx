import React from 'react';
import { Badge } from 'react-bootstrap';
import MdMessage from 'react-icons/lib/md/message';
import TiTrash from 'react-icons/lib/io/android-delete';
import Clock from 'react-icons/lib/md/access-time';
import MuteNotification from 'react-icons/lib/md/notifications-off';
import SearchIcon from 'react-icons/lib/md/search'


const Header = (props) => {
  const messageFromProps = props.messages.messages;
  let notReadMessages = messageFromProps.filter(message => message.read === false);
  // console.log('novi niz u headeru je', notReadMessages);
  return (
    <div className=" row col-md-12 col-sm-12 col-xs-12 padding-left-0 header-border max-width-header header-position row header-shadow  center-shadow">
      <div className="padding-header-bottom-left header-left-side header-border-bottom " >
        <div className="header-input-field-div ">
          {/* <input type="text" className="header-input-field" placeholder="Search" />  */}
          <div className="box">
            <div className="container-1">
              <span className="icon"><SearchIcon size="20" className="icon"/></span>
              <input type="search" id="search" placeholder="Search..." />
            </div>
          </div>
        </div>
      </div >
      <div className="float-header row " >
        <div className="col-md-4 col-sm-4 col-lg-4 col-xs-4"></div>
        <div className="col-md-4 col-sm-4 col-lg-4 col-xs-4"></div>
        <div className="col-md-4 col-sm-4 col-lg-4 col-xs-4 min-width">
          <div className=" col-md-4 col-sm-4 col-xs-4 "><MdMessage size="25" />
            <Badge>
              {notReadMessages.length}
            </Badge>
          </div>
          <div className=" col-md-4 col-sm-4 col-xs-4 " ><Clock size="25" /></div>
          <div className=" col-md-4 col-sm-4 col-xs-4"><TiTrash size="25" /></div>
        </div>
      </div>
    </div>
  );
}

export default Header;