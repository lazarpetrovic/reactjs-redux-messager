import React, { Component } from 'react';
import { FormGroup, InputGroup, FormControl } from 'react-bootstrap';
import TiAttachment from 'react-icons/lib/ti/attachment';
import Plus from 'react-icons/lib/io/android-add-circle';
import Mobile from 'react-icons/lib/io/android-phone-portrait';
import Arrow from 'react-icons/lib/io/arrow-up-b';


export class InputField extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(e) {
    // console.log(e.target.value);
    // console.log('ajmoooo', this.props.changeCurrentMsg)
    this.props.changeCurrentMsg(e.target.value);
  }
  onSubmit(event) {
    // console.log('onSubmit.....', this.props.trenutnaPoruka.currentMessage);

    event.preventDefault();
    this.props.addMsg(this.props.trenutnaPoruka.currentMessage);
    this.props.changeCurrentMsg('');
    document.getElementById('formId').value = '';
  }

  render() {
    return (
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-div max-width-header position-footer border row footer-border-top">
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-left-side bottom-left-div footer-left-side-div">
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 plus-div-style"><Plus size="40" className="plus"/></div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 mobile-div"><Mobile size="40" className="mobile"/></div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 inputField " >
            <form onSubmit={this.onSubmit}>
              <FormGroup>
                <InputGroup>
                <InputGroup.Addon><TiAttachment size="20" /></InputGroup.Addon>
                <FormControl id="formId" type="text" value={this.props.currentMessage} onChange={this.onChange} placeholder="Type a message..." />
                </InputGroup>
              </FormGroup>
            </form>
          </div>
      </div>
    )
  }
}