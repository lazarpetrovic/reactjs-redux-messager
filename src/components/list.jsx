import React, { Component } from 'react';
import '../main.css';
import { Button } from 'react-bootstrap';
import CloseButton from "react-icons/lib/md/close";
import Avatar from '../pic/avatar.png';
import TiTrash from 'react-icons/lib/io/android-delete';
import swal from 'sweetalert';


export default class List extends Component {
  onClickField(index) {
    this.props.readMessage(index);
  }
  onMouseEnter() {
    console.log('proslooo');
  }
  onClickButton(id) {
    // console.log('prolazi funkcija on click button', id);
    swal("Are you sure you want to delete?", {
      buttons: ["Cancel", true],
    }).then(value => {
      if (value) {
        this.props.deleteMessages(id);
      }
    });
    // this.props.deleteMessages(id);
  }
  render() {
    const mockedMessages = this.props.secondList.mockedMessages;
    const secondListItems = mockedMessages.map(message => {
      return (
        <div className=" col-lg-12 col-md-12 col-xs-12  col-sm-12 li-change-color">
          <div className="col-lg-3 col-md-3 col-xs-3 col-sm-3 ">
            <img src={message.imageUrl} alt="avatar" className="avatar" />
          </div>
          <div>
            <p className=" col-lg-9 col-md-9 col-xs-9 col-sm-9  p-padding">{message.name}</p><li className=" li-padding" key={message.id}>{message.message}</li>
          </div>
        </div>
      )
    })
    const testArray = this.props.messages.messages;
    // console.log('ovo je testArray', testArray);
    const listItem = testArray.map((message, index) => {
      if (message.read === false) {
        return (
          <div className="col-lg-12 col-md-12 col-xs-12  col-sm-12 row showTrash">
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <img src="https://scontent.fbeg5-1.fna.fbcdn.net/v/t1.0-1/p160x160/20479745_802748486563155_5947172641835093876_n.jpg?_nc_cat=0&oh=d114bbaba505bccd4ab5514a89f1bcc6&oe=5B2B7A55" alt="avatar" className="avatar img-padding" />
            </div>
            <div className=" list-style list-style col-lg-9 col-md-9 col-sm-9 col-xs-9 ">
              <div className="col-md-9 col-lg-9 col-sm-9 col-xs-9">
                <p className="bold-font">Lazar</p>
                <li key={message.id} onClick={this.onClickField.bind(this, index)} onMouseEnter={this.onMouseEnter.bind(this)}>
                  {message.message}
                </li>
              </div>
              <div className="col-md-3 col-lg-3 col-sm-3 col-xs-3 trashCan">
                <TiTrash  className="trash-pointer" onClick={this.onClickButton.bind(this, message.id)} />
              </div>
            </div>
          </div>
        )
      }
      else {
        return (
          <div className="col-lg-12 col-md-12 col-xs-12  col-sm-12 row showTrash">
          <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <img src="https://scontent.fbeg5-1.fna.fbcdn.net/v/t1.0-1/p160x160/20479745_802748486563155_5947172641835093876_n.jpg?_nc_cat=0&oh=d114bbaba505bccd4ab5514a89f1bcc6&oe=5B2B7A55" alt="avatar" className="avatar img-padding" />
          </div>
          <div className=" list-style list-style col-lg-9 col-md-9 col-sm-9 col-xs-9 ">
            <div className="col-md-9 col-lg-9 col-sm-9 col-xs-9">
              <p className="bold-font">Lazar</p>
              <li key={message.id} onClick={this.onClickField.bind(this, index)} onMouseEnter={this.onMouseEnter.bind(this)}>
                {message.message}
              </li>
            </div>
            <div className="col-md-3 col-lg-3 col-sm-3 col-xs-3 trashCan">
              <TiTrash  className="trash-pointer" onClick={this.onClickButton.bind(this, message.id)} />
            </div>
          </div>
        </div>
        )
      }
    }
    );
    return (
      <div className="padding-left-0 row" >
        <div className=" col-lg-6 col-md-6  col-sm-6 col-xs-6 fill-height right-padding-0 ">
          {secondListItems}
        </div>
        <div className="ulPading-div">
          <ul className="ulPading " >{listItem} </ul>
        </div>
      </div>


    )
  }
}