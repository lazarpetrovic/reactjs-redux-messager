export function fetchMessages() {
    return function (dispatch) {
        fetch(`http://localhost:4000/api/messages`)
            .then(function (response) {
                return response.json();
            }).then(function (data) {
                // console.log(data)
                dispatch({
                    type: 'FETCH_MESSAGES',
                    payload: data.data
                });
            })
    }
}


export function addNewMessage(message) {
    const msg = {
        message: {
            read: false,
            message: message
        }
    }
    return function (dispatch) {
        fetch(`http://localhost:4000/api/messages`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify(msg)
            })
            .then(function (response) {
                return response.json()
            })
            .then(function ({ data }) {
                dispatch({
                    type: "ADD_MESSAGE",
                    payload: data
                })
            })
    }
}

export function changeCurrentMesssage(message) {
    return {
        type: "CHANGE",
        payload: message
    }
}

export function readMessage(index) {
    return {
        type: 'TOGGLE_MESSAGE',
        payload: index
    }
}

export function allToRead() {
    return {
        type: 'ALLTOREAD'
    }
}

export function deleteMessages(id) {
    // console.log('ovoo je iddd', id);
    return function (dispatch) {
        fetch(`http://localhost:4000/api/messages/${id}`,
            {
                method: 'DELETE'
            })
            .then(function () {
                dispatch({
                    type: "DELETE_MESSAGE",
                    payload: id
                })
            })
    }
}
export function fetchMockedMesssages() {
    return function (dispatch) {
        fetch(`http://5ab4d4aaacec78001424aae9.mockapi.io/api/v1/Users`)
            .then(function (response) {
                return response.json();
                
            })
            .then(function (data) {
                // console.log('this is data', data);
                dispatch({
                    type: 'FETCH_MOCKED_MESSAGES',
                    payload: data
                });
            })
    }
}
