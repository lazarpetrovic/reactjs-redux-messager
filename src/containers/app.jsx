import React, { Component } from 'react';
import Header from '../components/header';
import { InputField } from '../components/input_field';
import List from '../components/list';
import { connect } from 'react-redux';
import { addNewMessage, changeCurrentMesssage, readMessage, allToRead, fetchMessages, deleteMessages, fetchMockedMesssages } from '../actions/messageActions';

class App extends Component {

  componentWillMount() {
    // console.log('pre renderovanja....', this.props.fetchMessages);
    this.props.fetchMessages();
    this.props.fetchMockedMesssages();
  }



  render() {
    return (
      <div className="mainDiv">
        <div>
          <Header messages={this.props.messages} />
          <List
            messages={this.props.messages}
            readMessage={this.props.readMessage}
            allToRead={this.props.allToRead}
            deleteMessages={this.props.deleteMessages}
            secondList={this.props.mockedMessages}
          />
          <InputField
            addMsg={this.props.addMsg}
            changeCurrentMsg={this.props.changeCurrentMsg}
            trenutnaPoruka={this.props.currentMessage} />
        </div>
      </div>

    );
  }
}


const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    currentMessage: state.currentMessage,
    mockedMessages: state.mockedMessages
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addMsg: (message) => {
      dispatch(addNewMessage(message));
    },
    changeCurrentMsg: (message) => {
      dispatch(changeCurrentMesssage(message));
    },
    readMessage: (index) => {
      dispatch(readMessage(index));
    },
    allToRead: () => {
      dispatch(allToRead());
    },
    fetchMessages: () => {
      dispatch(fetchMessages());
    },
    deleteMessages: (id) => {
      dispatch(deleteMessages(id));
    },
    fetchMockedMesssages: () => {
      dispatch(fetchMockedMesssages());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);